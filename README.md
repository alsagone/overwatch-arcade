# Overwatch Arcade

Displays Overwatch's current Arcade game modes - using https://overwatcharcade.today/ API

## Requirements
All the modules are in Python's basic installation but just to make sure :

Windows : ``` py -m pip json urllib requests sys ```
Linux/macOS : ``` python3 -m pip json urllib requests sys ```

## Usage
Windows : ``` py overwatchArcade.py ```
Linux/macOS : ``` python3 overwatchArcade.py ```
