import json
import urllib
from urllib.request import urlopen
import requests
import sys

def modeToStr(modeName, nbPlayers, recurrence):
    s = modeName + " - " + nbPlayers 

    if (recurrence != ""):
        s += " - " + recurrence

    return s

def displayArcadeModes():
    url = "https://overwatcharcade.today/api/today"
    infos = requests.get(url)

    try:
        rjson = json.loads(infos.text)
        
        tiles = ["tile_daily", "tile_weekly_1", "tile_weekly_2", "tile_large", "tile_permanent"]

        for t in tiles:
            modeName = rjson[t]["name"]
            nbPlayers = rjson[t]["players"]

            if ("weekly" in t):
                recurrence = "Changes weekly"
            elif ("daily" in t):
                recurrence = "Changes daily"
            elif ("permanent" in t):
                recurrence = "Permanent"
            else:
                recurrence = ""
            
            print(modeToStr(modeName, nbPlayers, recurrence))

        print("Last updated : " + rjson["updated_at"])

    except: 
        print("Error")
        sys.exit(1) 

    return

displayArcadeModes()
